import time
import random
import merge_sort

n = 1000

results_wc = []

while n <= 1000000:
    
    trial_wc = list(reversed(range(0, n + 1)))
    
    print("For " + str(n) + " items, the processing time in seconds is:")
    
    start = time.time()
    
    merge_sort.merge_sort(trial_wc)
    
    end = time.time()
    
    total_time = end - start
    
    results_wc.append(total_time)
    
    print(total_time)
    
    n = n * 2

# ---------------------------------------------------------------------------------------------------------------------------

# Sorted / ordered list: The items inside the list are already arranged in their correct order - from smallest to largest - theoretically, this represents a "best case" for the algorithm.

n = 1000

results_bc = []

while n <= 1000000:
    
    trial_bc = list(range(0, n + 1))
    
    print("For " + str(n) + " items, the processing time in seconds is:")
    
    start = time.time()
    
    merge_sort.merge_sort(trial_bc)
    
    end = time.time()
    
    total_time = end - start
    
    results_bc.append(total_time)
    
    print(total_time)
    
    n = n * 2
    
# ---------------------------------------------------------------------------------------------------------------------------

# Randomly shuffled list: Here, the numbers are randomly sorted - this is perceived theoretically as an "average case". However, the results point out that this is the worst case for the mergeSort function.
# This can be observed in the plot.

n = 1000

results_ac = []

while n <= 1000000:
    
    trial_ac = list(range(0, n + 1))
    
    print("For " + str(n) + " items, the processing time in seconds is:")
    
    random.shuffle(trial_ac)
    
    start = time.time()
    
    merge_sort.merge_sort(trial_ac)
    
    end = time.time()
    
    total_time = end - start
    
    results_ac.append(total_time)
    
    print(total_time)
    
    n = n * 2

# ---------------------------------------------------------------------------------------------------------------------------

# Visualization

m = 1000

items = []

while m <= 1000000:
    
    items.append(m)
    
    m = m * 2
    
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

plt.plot(items, results_ac, marker = ".", label = "Random shuffle case - Worst")
plt.plot(items, results_bc, marker = ".", label = "Sorted list")
plt.plot(items, results_wc, marker = ".", label = "Reversed list")


plt.xticks([1000, 2000, 4000, 8000, 16000, 32000, 64000, 128000, 256000, 512000, 1000000], ["1k", "2k", "4k", "8k", "16k", "32k", "64k", "128k", "256k", "512k", "1M"])
plt.xlabel("n")
plt.ylabel("Time in seconds")
plt.legend()
plt.show()
